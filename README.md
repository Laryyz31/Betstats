# Betstats

Web application which keeps you updated of games in various leagues(atm. NHL, Major Soccer leagues in Europe)


### Prerequisities

Our application runs inside of docker container, so docker should be enough.

Ubuntu
```
sudo apt-get install docker-engine
```
CentOs/RHEL
```
sudo yum install docker-engine
```
Debian
```
sudo apt-get install docker-engine
```
Windows

https://docs.docker.com/docker-for-windows/

### Installing

A step by step series of examples that tell you have to get a development env running

First step is to start docker service.

```
sudo service docker start
```

And build image:

```
docker build -t=docker-betstats
```

And then run container:

```
docker run -p 3000:3000 -i -t docker-betstats
```

Now you should see our application in localhost:3000

## Running the tests

Unit tests are going to be ran automaticly in Jenkins.


## Built With

Does not require building yet.

## Tools & Frameworks

- HTML
- CSS
- JavaScript
- JQUERY
- MySQL
- PHP
