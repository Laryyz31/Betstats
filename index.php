<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>BetStats™</title>

    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link rel="stylesheet" href="font-awesome-4.6.3/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
    <script src="js/jquery-3.1.0.min.js"></script>
    <script src="js/basicscript.js"></script>
    <script type="text/javascript" src="js/ajaxcalls.js"></script>

</head>

<body>

<!-- login and register pop-ups -->
<div id="logbg"></div>
<div id="logwd">
    <h2>Login</h2>
    <form>
        <input type="text" name="username" placeholder="Username"/>
        <input type="password" class="pswd" name="password" placeholder="Password"/>
        <input type="submit" value="Submit"/>
    </form>
</div>
<div id="regwd">
    <h2>Register</h2>
    <form>
        <input type="text" name="rusername" placeholder="Username"/>
        <input type="email" name="email" placeholder="E-mail"/>
        <input type="password" class="pswd" name="password1" placeholder="Password"/>
        <input type="password" class="pswd" name="password2" placeholder="Repeat password"/>
        <input type="submit" value="Submit"/>
    </form>
</div>

<!-- login register and search -->
<header>
    <h1>BetStats™</h1>
    <input type="text" name="search" placeholder="Search..." /><a href="#" id="search" title="Search"><i class="fa fa-search"></i></a>
    <div id="lgrg"> <a href="#" id="login" title="Login"><i class="fa fa-sign-in"></i></a> <a href="#" id="register" title="Register"><i
                class="fa fa-user-plus"></i></a></div>
</header>

<!-- header image -->
<figure id="hrd">
    <img src="img/soccer_v2.png" alt="Football players running on a field. Photo by Abigail Keenan"/>
</figure>

<!-- tabs -->
<nav>
    <a href="#" onclick="openTab(event, 'pl')">Premier League</a>
    <a href="#" onclick="openTab(event, 'efl');efl()">EFL Championship</a>
    <a href="#" onclick="openTab(event, 'l1');l1()">EFL League One</a>
</nav>

<!-- main body -->
<article id="pl">
    <h3>Latest Matches</h3>
    <table id="score_pl">
        <tr>
            <th>Date</th>
            <th>Home</th>
            <th>Away</th>
            <th>Score</th>
        </tr>
    </table>

    <h3>League Table</h3>
    <table id="league_pl">
        <tr>
            <th>#</th>
            <th>Team</th>
            <th>Matches</th>
            <th>Points</th>
        </tr>
    </table>
</article>

<article id="efl">
    <h3>Latest Matches</h3>
    <table id="score_efl">
        <tr>
            <th>Date</th>
            <th>Home</th>
            <th>Away</th>
            <th>Score</th>
        </tr>
    </table>

    <h3>League Table</h3>
    <table id="league_efl">
        <tr>
            <th>#</th>
            <th>Team</th>
            <th>Matches</th>
            <th>Points</th>
        </tr>
    </table>
</article>

<article id="l1">
    <h3>Latest Matches</h3>
    <table id="score_l1">
        <tr>
            <th>Date</th>
            <th>Home</th>
            <th>Away</th>
            <th>Score</th>
        </tr>
    </table>

    <h3>League Table</h3>
    <table id="league_l1">
        <tr>
            <th>#</th>
            <th>Team</th>
            <th>Matches</th>
            <th>Points</th>
        </tr>
    </table>
</article>

</body>
</html>