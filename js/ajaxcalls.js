var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://api.football-data.org/v1/competitions/426/fixtures",
    "method": "GET",
    "dataType" : "JSON",
    "headers": {
        "x-auth-token": "f66f53c19aab4f128619de3b2622bab8"
    }
}

var done_1 = false;
var done_2 = false;

$.ajax(settings).done(function (response) {
    var scores = [];
    var a = 0;
    var b = response.fixtures.length - 1;
    while (a < 10) {
        if (response.fixtures[b].status == 'FINISHED') {
            scores.push(response.fixtures[b]);
            a++;
            b--;
        } else {
            b--;
        }
    }
    console.log(scores);
    for (var i = 0; i < scores.length; i++) {
        var date = scores[i].date.substring(8,10) + '.' + scores[i].date.substring(5,7) + '.' + scores[i].date.substring(0,4);
        var home = scores[i].homeTeamName;
        var away = scores[i].awayTeamName;
        var score = scores[i].result.goalsHomeTeam + ' - ' + scores[i].result.goalsAwayTeam;
        var tr = document.createElement('tr');
        var td = [document.createElement('td'),
            document.createElement('td'),
            document.createElement('td'),
            document.createElement('td')];
        td[0].innerHTML = date;
        if (scores[i].result.goalsHomeTeam > scores[i].result.goalsAwayTeam) {
            td[1].innerHTML = '<b>' + home + '</b>';
            td[2].innerHTML = away;
        } else if (scores[i].result.goalsHomeTeam < scores[i].result.goalsAwayTeam) {
            td[2].innerHTML = '<b>' + away + '</b>';
            td[1].innerHTML = home;
        } else {
            td[1].innerHTML = home;
            td[2].innerHTML = away;
        }
        td[3].innerHTML = score;
        for (var a = 0; a < 4; a++) {
            tr.appendChild(td[a]);
        }
        document.getElementById('score_pl').appendChild(tr);

    }
});

settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://api.football-data.org/v1/competitions/426/leagueTable",
    "method": "GET",
    "dataType" : "JSON",
    "headers": {
        "x-auth-token": "f66f53c19aab4f128619de3b2622bab8"
    }
}

$.ajax(settings).done(function (response) {
    var scores = response.standing.slice(0,10);
    for (var i = 0; i < scores.length; i++) {
        var num = scores[i].position;
        var name = '<div class="legimg"><img src="' + scores[i].crestURI + '" /></div>' + scores[i].teamName;
        var match = scores[i].playedGames;
        var point = scores[i].points;
        var tr = document.createElement('tr');
        var td = [document.createElement('td'),
            document.createElement('td'),
            document.createElement('td'),
            document.createElement('td')];
        td[0].innerHTML = num;
        td[1].innerHTML = name;
        td[2].innerHTML = match;
        td[3].innerHTML = point;
        for (var a = 0; a < 4; a++) {
            tr.appendChild(td[a]);
        }
        document.getElementById('league_pl').appendChild(tr);

    }
});

function efl() {
    if(!done_1) {

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://api.football-data.org/v1/competitions/427/fixtures",
            "method": "GET",
            "dataType": "JSON",
            "headers": {
                "x-auth-token": "f66f53c19aab4f128619de3b2622bab8"
            }
        }

        $.ajax(settings).done(function (response) {
            var scores = [];
            var a = 0;
            var b = response.fixtures.length - 1;
            while (a < 10) {
                if (response.fixtures[b].status == 'FINISHED') {
                    scores.push(response.fixtures[b]);
                    a++;
                    b--;
                } else {
                    b--;
                }
            }
            console.log(scores);
            for (var i = 0; i < scores.length; i++) {
                var date = scores[i].date.substring(8, 10) + '.' + scores[i].date.substring(5, 7) + '.' + scores[i].date.substring(0, 4);
                var home = scores[i].homeTeamName;
                var away = scores[i].awayTeamName;
                var score = scores[i].result.goalsHomeTeam + ' - ' + scores[i].result.goalsAwayTeam;
                var tr = document.createElement('tr');
                var td = [document.createElement('td'),
                    document.createElement('td'),
                    document.createElement('td'),
                    document.createElement('td')];
                td[0].innerHTML = date;
                if (scores[i].result.goalsHomeTeam > scores[i].result.goalsAwayTeam) {
                    td[1].innerHTML = '<b>' + home + '</b>';
                    td[2].innerHTML = away;
                } else if (scores[i].result.goalsHomeTeam < scores[i].result.goalsAwayTeam) {
                    td[2].innerHTML = '<b>' + away + '</b>';
                    td[1].innerHTML = home;
                } else {
                    td[1].innerHTML = home;
                    td[2].innerHTML = away;
                }
                td[3].innerHTML = score;
                for (var a = 0; a < 4; a++) {
                    tr.appendChild(td[a]);
                }
                document.getElementById('score_efl').appendChild(tr);

            }
        });

        settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://api.football-data.org/v1/competitions/427/leagueTable",
            "method": "GET",
            "dataType": "JSON",
            "headers": {
                "x-auth-token": "f66f53c19aab4f128619de3b2622bab8"
            }
        }

        $.ajax(settings).done(function (response) {
            var scores = response.standing.slice(0, 10);
            for (var i = 0; i < scores.length; i++) {
                var num = scores[i].position;
                var name =  scores[i].teamName;
                var match = scores[i].playedGames;
                var point = scores[i].points;
                var tr = document.createElement('tr');
                var td = [document.createElement('td'),
                    document.createElement('td'),
                    document.createElement('td'),
                    document.createElement('td')];
                td[0].innerHTML = num;
                td[1].innerHTML = name;
                td[2].innerHTML = match;
                td[3].innerHTML = point;
                for (var a = 0; a < 4; a++) {
                    tr.appendChild(td[a]);
                }
                document.getElementById('league_efl').appendChild(tr);

            }
        });
        done_1 = true;
    }
}

function l1() {
    if(!done_2) {

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://api.football-data.org/v1/competitions/428/fixtures",
            "method": "GET",
            "dataType": "JSON",
            "headers": {
                "x-auth-token": "f66f53c19aab4f128619de3b2622bab8"
            }
        }

        $.ajax(settings).done(function (response) {
            var scores = [];
            var a = 0;
            var b = response.fixtures.length - 1;
            while (a < 10) {
                if (response.fixtures[b].status == 'FINISHED') {
                    scores.push(response.fixtures[b]);
                    a++;
                    b--;
                } else {
                    b--;
                }
            }
            console.log(scores);
            for (var i = 0; i < scores.length; i++) {
                var date = scores[i].date.substring(8, 10) + '.' + scores[i].date.substring(5, 7) + '.' + scores[i].date.substring(0, 4);
                var home = scores[i].homeTeamName;
                var away = scores[i].awayTeamName;
                var score = scores[i].result.goalsHomeTeam + ' - ' + scores[i].result.goalsAwayTeam;
                var tr = document.createElement('tr');
                var td = [document.createElement('td'),
                    document.createElement('td'),
                    document.createElement('td'),
                    document.createElement('td')];
                td[0].innerHTML = date;
                if (scores[i].result.goalsHomeTeam > scores[i].result.goalsAwayTeam) {
                    td[1].innerHTML = '<b>' + home + '</b>';
                    td[2].innerHTML = away;
                } else if (scores[i].result.goalsHomeTeam < scores[i].result.goalsAwayTeam) {
                    td[2].innerHTML = '<b>' + away + '</b>';
                    td[1].innerHTML = home;
                } else {
                    td[1].innerHTML = home;
                    td[2].innerHTML = away;
                }
                td[3].innerHTML = score;
                for (var a = 0; a < 4; a++) {
                    tr.appendChild(td[a]);
                }
                document.getElementById('score_l1').appendChild(tr);

            }
        });

        settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://api.football-data.org/v1/competitions/428/leagueTable",
            "method": "GET",
            "dataType": "JSON",
            "headers": {
                "x-auth-token": "f66f53c19aab4f128619de3b2622bab8"
            }
        }

        $.ajax(settings).done(function (response) {
            var scores = response.standing.slice(0, 10);
            for (var i = 0; i < scores.length; i++) {
                var num = scores[i].position;
                var name =  scores[i].teamName;
                var match = scores[i].playedGames;
                var point = scores[i].points;
                var tr = document.createElement('tr');
                var td = [document.createElement('td'),
                    document.createElement('td'),
                    document.createElement('td'),
                    document.createElement('td')];
                td[0].innerHTML = num;
                td[1].innerHTML = name;
                td[2].innerHTML = match;
                td[3].innerHTML = point;
                for (var a = 0; a < 4; a++) {
                    tr.appendChild(td[a]);
                }
                document.getElementById('league_l1').appendChild(tr);

            }
        });
        done_2 = true;
    }
}