$(document).ready(function () {
    document.getElementById('pl').style.display = "block";
    document.getElementsByTagName('NAV')[0].getElementsByTagName('A')[0].className += "active";

    // login and register pop-ups
    $('#logbg').click(function () {
        $('#logbg').fadeOut();
        $('#logwd').fadeOut();
        $('#regwd').fadeOut();
    })

    $('#login').click(function () {
        $('#logbg').fadeIn();
        $('#logwd').fadeIn()
    })

    $('#register').click(function () {
        $('#logbg').fadeIn();
        $('#regwd').fadeIn();
    })

})

// navigation tabs
function openTab(evt, tabNum) {
    var i, tabcontent, nav, tab;

    tabcontent = document.getElementsByTagName("ARTICLE");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    nav = document.getElementsByTagName("NAV")[0];
    tab = nav.getElementsByTagName("A");
    for (i = 0; i < tab.length; i++) {
        tab[i].className = tab[i].className.replace("active", "");
    }

    document.getElementById(tabNum).style.display = "block";
    evt.currentTarget.className += "active";
}
